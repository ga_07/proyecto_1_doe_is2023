# Proyecto_1_DOE_IS2023



## Proyecto 1 Diseño de experimentos: Galton Board

Estudiantes:

- [ ] Gabriel Gutiérrez
- [ ] Joseph González

## Documentos

Dentro de este repositorio se encuentran los archivos necesarios para el análisis estadísitico para este proyecto

- [ ] Datos recopilados: Datos_Proyecto_1.ods 
- [ ] Sript de R comentado generado en R: Proyecto_1.R
- [ ] Imagénes recopiladas de el ánalisis estadístico

